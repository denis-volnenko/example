package ru.volnenko.example.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

@WebService
public class CalculatorEndpoint {

    public static final String URL = "http://0.0.0.0:8080/CalculatorEndpoint?wsdl";

    @WebMethod
    public int sum(
            @WebParam(name = "a") int a,
            @WebParam(name = "b") int b
    ) {
        return a + b;
    }

    @WebMethod
    public int abs(@WebParam(name = "value") int value) {
        return Math.abs(value);
    }

}