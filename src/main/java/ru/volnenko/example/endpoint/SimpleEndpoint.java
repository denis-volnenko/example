package ru.volnenko.example.endpoint;

import ru.volnenko.example.model.Simple;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

@WebService
public class SimpleEndpoint {

    public static final String URL = "http://0.0.0.0:8080/SimpleEndpoint?wsdl";

    private EntityManagerFactory factory;

    {
        factory = Persistence.createEntityManagerFactory("MYSQL");
    }

    @WebMethod
    public List<Simple> findAll() {
        final EntityManager em = factory.createEntityManager();
        final List<Simple> result = em
                .createQuery("FROM Simple", Simple.class)
                .getResultList();
        em.close();
        return result;
    }

    @WebMethod
    public Simple create() {
        final EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();

        final Simple simple = new Simple();
        simple.setName("NEW: " + System.nanoTime());
        em.persist(simple);

        em.getTransaction().commit();
        em.close();
        return simple;
    }

}
