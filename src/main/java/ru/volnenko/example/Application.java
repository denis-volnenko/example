package ru.volnenko.example;

import ru.volnenko.example.endpoint.CalculatorEndpoint;
import ru.volnenko.example.endpoint.SimpleEndpoint;

import javax.xml.ws.Endpoint;

public class Application {

    public static void main(String[] args) {
        final SimpleEndpoint simpleEndpoint = new SimpleEndpoint();
        Endpoint.publish(SimpleEndpoint.URL, simpleEndpoint);
        System.out.println(SimpleEndpoint.URL);

        final CalculatorEndpoint calculatorEndpoint = new CalculatorEndpoint();
        Endpoint.publish(CalculatorEndpoint.URL, calculatorEndpoint);
        System.out.println(CalculatorEndpoint.URL);
    }

}
